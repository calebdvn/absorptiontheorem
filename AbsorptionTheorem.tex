\documentclass{article}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{parskip}
\title{A Proof of the Absorption Theorem}
\author{Caleb Devine and Kevin Milans}
\date{August 09, 2018}

\newcommand{\A}{$\mathcal{A}$}
\newcommand{\B}{$\mathcal{B}$}
\newcommand{\C}{$\mathcal{C}$}
\newcommand{\F}{$\mathcal{F}$}
\newcommand{\G}{$\mathcal{G}$}
\renewcommand{\H}{$\mathcal{H}$}
\newcommand{\X}{$\mathcal{X}$}
\newcommand{\Y}{$\mathcal{Y}$}
\newcommand{\Z}{$\mathcal{Z}$}
\newcommand{\Aa}{\mathcal{A}}
\newcommand{\Bb}{\mathcal{B}}
\newcommand{\Cc}{\mathcal{C}}
\newcommand{\Dd}{\mathcal{D}}
\newcommand{\Ff}{\mathcal{F}}
\newcommand{\Gg}{\mathcal{G}}
\newcommand{\Hh}{\mathcal{H}}
\newcommand{\Xx}{\mathcal{X}}
\newcommand{\Yy}{\mathcal{Y}}
\newcommand{\Zz}{\mathcal{Z}}
\newcommand{\s}{\ $\propto$\ }
\newcommand{\Ss}{\ \propto\ }

\newtheorem{thm}{Theorem}
\newtheorem{cor}{Corollary}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}
\newtheorem{remark}{Remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let \A, \B, and $\Aa_1,\Aa_2,\ldots$ be nonempty disjoint union closed families. Define the following:
\begin{itemize}
    \item \A\ {\bf absorbs} \B\ iff for every $\forall a\in$\A, $\forall b\in$\B, $a\cup b\in$\A
    \item ($\Aa_1,\ldots,\Aa_n$) is an {\bf absorption chain} iff  for each $j\ge i$, $\Aa_i$ absorbs $\Aa_j$. Each $\Aa_i$ is a {\bf link}. $\Aa_1$ is the {\bf top link}. $\Aa_n$ is the {\bf bottom link}.
    
    \emph{\footnotesize (Assume the absorption chains considered in this paper have no cloned elements.)}
    \item $x$ is {\bf frequent} in \A\ iff it appears in at least half of \A.
    \item $x$ is {\bf frequent} in an absorption chain iff $x$ is frequent in each link and appears in over half the the sets of the total family.
    \item $x$ is {\bf dominant} in \A\ iff every set in \A\ contains $x$
    \item \F\ is the {\bf Total Family} of $\chi$ iff $\Ff=\bigcup\limits_{i=1}^{n}\Aa_i$.
    \item $\{S,S\backslash x\}$, is an {\bf x-pair} in \F\ iff $S\in\Ff$ and $S\backslash\text{x}\in\Ff$
    \item If $\chi=(\Aa_1,\ldots,\Aa_n)$ is an absorption chain, then $x$ is a {\bf decisive element} of $\Aa_i$ iff $x$ dominates $\Aa_i$, and is in any set any link, $\Aa_j$, with $j>i$.
    \item $(x_1,\ldots,x_n)$ is a {\bf decisive sequence} of absorption chain, $\chi=(\Aa_1,\ldots,\Aa_n)$ iff $x_i$ is a decisive element of $\Aa_i$ for each $i$.
    \item $T_i$ is the {\bf big set} of family $\Aa_i$ iff $T_i$ is the union of all sets in $\Aa_i$.
\end{itemize}
~

\begin{thm}[Absorption Theorem]
    Every element in the bottom link of a maximum length absorption chain, $\chi$, is frequent in $\chi$
\end{thm}
~


\begin{prop} If $\chi=(\Aa_1,\Aa_2,\ldots,\Aa_n)$ is an absorption chain, then $\chi$ is completely determined by any decisive sequence of $\chi$. 
\end{prop}
\begin{proof}
Let \F\ be the total family of $\chi$, and $(x_1,\ldots,x_n)$ be a decisive sequence. $\chi$ is completely defined by: $\Aa_i=\{A\in\Ff:x_i\in A\text{ and }(\forall j<i) x_j\not\in A\}$
\end{proof}
~

\begin{prop}
    If $\chi$ is an absorption chain, and $S$ is an inclusion minimal set in link $\Aa_i$ that has size at least 2, then $(\chi-S)$ is an absorption chain.
\end{prop}
\begin{proof}
    Since $\chi$ is an absorption chain, it suffices to show the total family of $(\chi-S)$ is union closed. Let $f_1,f_2\in\Ff$. Since $\chi$ is an absorption chain, $f_1\cup f_2=S$ only if either $f_1$ or $f_2$ is contained in $\Aa_i$. Without loss of generality, assume $f_1\in\Aa_i$. Since $S$ is inclusion minimal in $\Aa_i$, $f_1\cup f_2=S$ only if $f_1=S$. Since $f_1\not=S$, $(\chi-S)$ is union closed.
\end{proof}

\begin{lem}
    In any minimum counter example to the absorption theorem, every link except for $\Aa_1$ has size 1.
\end{lem}
\begin{proof}
    Suppose $\chi=(\Aa_1,\ldots,\Aa_n)$ is a minimum counter example to the absorption theorem, and $\Aa_1$ has size 1. $(\chi-\Aa_1)$ is a maximum chain. So, inductively, every element in $\Aa_n$ is frequeint in $(\chi-\Aa_1)$. Since the singleton set in $\Aa_1$ contains every element of $\Aa_n$, every element in $\Aa_n$ is frequent in $\chi$.

    Supose for some link of $\chi$, say $\Aa_j$, both $\Aa_1$ and $\Aa_j$ have size larger than 1. Let $S_1$ be any inclusion minimal set in $\Aa_1$, and let $S_j$ be any inclusion minimal set in $\Aa_j$. The chain constructed by deleting $S_1$ from $A_1$ in $\chi$ inductively has that every element in the bottom link is frequent. Thus, every element in the bottom link of $\chi$ is frequent in each link of $\chi$ except possibly $\Aa_1$. The chain constructed by deleting $S_j$ from $\Aa_j$ inductively has that every element in the bottom link is frequent in each link, notably $\Aa_1$. Therefor every element in the bottom link of $\chi$ is frequent in $\Aa_1$. So the $\chi$ is not a mimimum counter example.
\end{proof}

\begin{lem}
Let $\chi=(\Aa_1,\Aa_2,\ldots,\Aa_n)$ be on total family \F, be a minimum counter example to the absorption theorem. Then, \F\ has at most 1 absorption chain of length $n$ whose bottom link is $\Aa_n$.
\end{lem}
\begin{proof}
Let $\chi_1=(\Aa_1,\ldots,\Aa_n)$ and $\chi_2=(\Bb_1,\ldots,\Bb_n)$ be two maximum length absorption chains on \F\ with bottom link, $\Aa_n$. Let $\Aa_j$ be the link of smallest index $j$ which both $\chi_1$ and $\chi_2$ contain. For all $i\ge j$, $\Bb_i = \Aa_i$. 

Let $\chi_B$ be constructed from $\chi_2$ by replacing $\Bb_1$ with $\Bb_1\cap\Aa_1$. Since $\chi_1\not=\chi_2$, the total family of $\chi_B$ contains fewer sets than the total family of $\chi_2$. Inductively, every element in $\Aa_n$ is frequent in $\chi_B$. Therefore, since $\Aa_1=(\Bb_1\cap\Aa_1)\cup(\Bb_2\cup\Bb_3\ldots\cup\Bb_n)$, the elements of $\Aa_n$ are frequent in $\Aa_1$.
\end{proof}

\begin{lem}
A minimum counter example to the absorption theorem has length at least 3.
\end{lem}
\begin{proof}
The top link of any maximum chain of length at most 2 contains every element and each element is missing from at most 1 set.
\end{proof}
~

\begin{lem}
Let $\chi=(\Aa_1,\Aa_2,\ldots,\Aa_n)$ be a maximum length absorption chain. $\Aa_1$ contains an element, $x_1$, which is in none of the other links. If $\chi$ is a minimum counter example to the absorption theorem, then for each $i$ with $i\ge 3$, the big set, $T_i$ of link $\Aa_i$, is not a member of any $x_1$-pair in \F. Additionally, $\{T_1,T_2\}$ is an $x_1$-pair in \F.
\end{lem}
\begin{proof}
If \F\ contains an $x_1$-pair for some $T_i$, say $T_j$, then \F contains another chain ending in $\Aa_n$. The chain is constructed by reordering the decisive sequence on \F. Let $(x_1,x_2,\ldots,x_n)$ be the decisive sequence of $\chi$. The chain, $\chi'$, determined by the decisive sequence, $(x_2,x_3,\ldots,x_{j-1},x_j,x_1,x_{j+1},x_{j+2},\ldots,x_n)$ is another length $n$ absorption chain ending in $\Aa_n$. 

Since $x_2,\ldots,x_j$ determines links in $\chi$ which are nonempty, clearly the links in $\chi'$ determined by $x_2,\ldots,x_j$ are also nonempty. It remains to show that in $\chi'$ the link determined by $x_1$ is nonempty. 

Since $T_j$ is in an $x_1$-pair, it follows that $T_j\cup\{x_1\}$ is a member of the link in $\chi'$ determined by $x_j$. Thus, $T_j$ is a member of the link determined by $x_1$ in $\chi'$, so that link is nonempty. in $\chi'$ is By Lemma 2, in a minimum counter example, there can only exist 1 maximum length absorption chain ending in $\Aa_n$. Contradiction.

Since $\chi$ is a maximum chain, every element not in $T_2$ must be contained in every set of $\Aa_1$. But, since we assume there are no cloned elements, there is at most one element contained in every set of $\Aa_1$. Thus, since $T_2$ lacks $x_1$, $x_1$ is the only element $T_2$ lacks. Therefore $T_2\cup\{x_1\}=T_1$. So, $\{T_1,T_2\}$ is an $x_1$-pair.
\end{proof}
~

\begin{lem}
Let $T$ be big set in \F. Let \G\ be the family constructed by deleting $x$ from every set in \F, except $T$. If \F\ contains no $x$-pair other than an $x$-pair which contains $T$, then \G\ is a union closed family. Additionally, any maximum absorption chain in \G\ corresponds to a maximum absorption chain in \F\ with the same decisive sequence.
\end{lem}
\begin{proof}
~\newline
First it will be shown that \G\ is union closed. Let $f_1$ and $f_2$ be two sets in \F. Since \F\ is union closed, \F contains $f_1\cup f_2$. Let $g_1=f_1-x$ and $g_2=f_2-x$. Since $f_1$ and $f_2$ are not specified to contain $x$, $g_1$ may equal $f_1$, and likewise $g_2$ may equal $f_2$. If $f_1\cup f_2=T$, then $g_1\cup g_2=(T-x)$ unless one of $g_1$ or $g_2$ is $T$. Either way, since $T$ and $T-x$ exist in \G, both unions exist in \G. If $(f_1\cup f_2) \not= T$. Since $g_1\cup g_2 =((f_1-x)\cup (f_2-x)) = (f_1\cup f_2)-x$ and $(f_1 \cup f_2)$ is contained in \F, it follows that the union of any $g_1$ and $g_2$ will be contained in \G. Thus, \G\ is union closed.

$(\star)$ Let \G' be a union closed subset of \G. Let $\chi'$ be any absorption chain on \G' determined by the decisive sequence $(y_1,\ldots,y_k)$. If every link in $\chi'$ contains at least one set which is disjoint from $\{T_1,\ldots,T_n\}$, then chain in \G\ determined by the decisive sequence, $(y_1,y_2,\ldots,y_k)$, is an absorption chain in \F\ whose decisive elements are the same as the decisive elements of $\chi'$

Any absorption chain, $\chi_G$, in \G\ can be expressed as the concatenation of a prefix absorption chain and suffix absorption chain, where the prefix absorption chain contains no link that consists exclusively of subsets of $T_3$, and the suffix absorption chain has all its links consist entirely of subsets of $T_3$. By $(\star)$, the decisive sequence of the prefix absorption chain determines an absorption chain in \F. The suffix is also contained in \F. Therefore, the entire decisive sequence of the absorption chain of $\chi_G$ is a decisive sequence for an absorption chain in \F.
\end{proof}

\begin{lem}
Let $\chi=(\Aa_1,\ldots,\Aa_n)$ be a minimum counter example to the Absorption Theorem. If the only $x_1$-pair amongst all links in $\chi$ includes the big set of $\Aa_1$, then every element in $\Aa_n$ is frequent in $\Aa_1$.
\end{lem}
\begin{proof}
Let \G\ be defined as in Lemma 5. For each $i$, let $T_i$ be the big set of $\Aa_i$. Observe that the $\chi$'s decisive sequence determines an absorption chain in \G. Let $\chi'=(\Bb_1,\Bb_2,\ldots,\Bb_{n'})$ be an absorption chain of maximum length, $n'\ge n$, on \G. Let $(b_{1},b_{2},\ldots,b_{n'})$ be a decisive sequence for $\chi'$. For each $i$, let $U_i$ be the big set of $\Bb_i$. Since $\{T_1,T_2\}$ is the only $x$-pair in \F, \F\ contains no sets of the form $Y\cup\{x\}$ where $Y$ is a subset of $T_3$. Otherwise, the set $\{T_3\cup Y\cup \{x\},T_3\}$ is an $x$-pair. Therefore \G\ contains no subset of $T_3$ other than $\{T_3,\ldots,T_n\}$. 

By Lemma 5, the chain $\chi_G=(\Hh_1,\ldots,\Hh_n)$, on \G, which is determined by the same decisive sequence as $\chi$, is of maximum length. Additionally, since \G\ contains no subsets of $T_3$ other than $T_i$ for $i\ge 3$, and $\Hh_3=\{T_3\}$, it follows that all of the sets in \G\ of the form $(S-x)$ for $S\in\Aa_1$ must  be contained in either $\Hh_1$ or $\Hh_2$. However, all $S\in\Hh_1$ contain $x$. Therefore, all sets in \G\ of the form, $(S-x)$ for $S\in\Aa_1$, must be contained in $\Hh_2$. $\Hh_2$ contains no other sets. Therefore $\Hh_2=\{(S-x):S\in\Aa_1\}$, and $\Hh_1=\{T_1\}$. Thus, the chain constructed by deleting $\Hh_1$ from $\chi_G$ inductively has that every element in $\Hh_n$ frequent in $\Hh_2$. Since $\Hh_n=\{T_n\}$, it follows all elements in $\Aa_n$ are frequent in $\Hh_2$, and therefore are also frequent in $\Aa_1$.
\end{proof}
~

\begin{proof}[{\bf Proof of The Absorption Theorem}]~\newline\newline
Suppose a maximum length absorption chain $\chi=(\Aa_1,\Aa_2,\ldots,\Aa_n)$ is a minimum counter example to the absorption theorem, and $x_1$ is a decisive element for $\Aa_1$. For each $\Aa_i$, let $T_i$ be the big set of $\Aa_i$. By Lemma 4, $\{T_1,T_2\}$ is the only $x_1$-pair. So, by lemma 6, all elements in $\Aa_n$ are frequent in $\Aa_1$. Thus, all elements in $\Aa_n$ are frequent in $\chi$.
\end{proof}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
